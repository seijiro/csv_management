<?php

class Validator
{
    /**
    ** 半角英数字のValidation
    ***/
    public function is_normal_width($text)
    {
        if (preg_match("/^[a-zA-Z0-9]+$/",$text)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }

    /**
    ** EmailのValidation
    ***/
    public function is_email($address){
        if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
            return TRUE;
        }
        return FALSE;
    }
}
