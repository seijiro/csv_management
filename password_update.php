<?php
    require_once(dirname(__FILE__) . "/module/csv_manager.php");
    require_once(dirname(__FILE__) . "/module/validate_module.php");
    require_once(dirname(__FILE__) . "/config.php");

    function error($param){
        header("Location: " .  "/admin.php" . "?error=" . $param);
        exit;
    }
    function redirect(){
        header("Location: " . "/admin.php?success=処理完了しました。");
        exit;
    }
    $manager = new CSVManager('metadata/data.csv');
    $email = $_POST["email"];
    $new_password = $_POST["new_password"];
    $validator = new Validator();
    $validate_flag = $validator->is_normal_width($new_password);
    if(!$validate_flag) {
        $msg = '半角英数字以外が含まれています';
        error($msg);
    }

    $csv_data = $manager->getCsvArray();
    $manager->updatePasswordFromEmail($email, $new_password, $csv_data);
    redirect();


