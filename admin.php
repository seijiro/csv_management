<?php
 require_once(dirname(__FILE__) . "/module/csv_manager.php");
 require_once(dirname(__FILE__) . "/config.php");
 $manager = new CSVManager(CSV_DIR);
 $csv = $manager->getCsvArray();
?>

<html lang="ja">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.13.0/build/cssreset/cssreset-min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
    <header class="aainc">
      <div class="wrap cf">
        <h1><a href="#">CSV管理ソフトウェア</a></h1>
      </div>
    </header>
    <div class="wrap">
     <?php if(isset($_GET['error'])): ?>
         <p class="boxError1">
           <?php echo $_GET['error']; ?>
         </p>
       <?php endif ?>
       <?php if(isset($_GET['success'])): ?>
         <p class="boxError1">
           <?php echo $_GET['success']; ?>
         </p>
       <?php endif ?>
       <table class="mBottom10">
        <thead>
          <tr>
            <th>Email検索</th>
            <th><input type="text" size="20" id="search"></th>
          </tr>
        </thead>
        <thead>
          <tr>
            <th>メールアドレス</th>
            <th>新しいパスワードを入力</th>
          </tr>
        </thead>
        <tbody id="search_area">
          <?php foreach($csv as $key => $colum): ?>
            <tr>
              <td><?php echo $colum[0]; ?></td>
              <td>
                <form id="admin_password_update" action="password_update.php" method="post">
                  <input id="password" type="text" name="new_password" size="40">
                  <input type="hidden" name="email" value=<?php echo "\"" . $colum[0] . "\""; ?>>
                  <input type="submit" value="送信">
                </form>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="js/html5shiv-printshiv.js" type="text/javascript"></script>
    <script src="js/unit.js" type="text/javascript"></script>
    <script src="js/validation.js" type="text/javascript"></script>
    <script src="js/search.js" type="text/javascript"></script>
  </body>
  </html>
