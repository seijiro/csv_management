<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.13.0/build/cssreset/cssreset-min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/default.css">
  </head>
  <body>
  <header class="aainc">
      <div class="wrap cf">
        <h1><a href="#">CSV管理ソフトウェア</a></h1>
      </div>
    </header>
  <div class="wrap">
    <form id="public_password_update" action="public_update_password.php" method="post">
      <table>
        <thead>
          <tr>
            <th>パスワード変更</th>
            <th>フォーム</th>
          </tr>
        </thead>
        <tr>
          <td>ID(メールアドレス)</td>
          <td><input type="text" name="email" id="email" id="email"></td>
        </tr>
        <tr>
          <td>旧パスワード</td>
          <td><input type="password" name="old_password" id="old_password"></td>
        </tr>
        <tr>
          <td>新パスワード</td>
          <td><input type="password" name="new_password" id="new_password"></td>
        </tr>
        <tr>
          <td><input type="submit" value="更新" ></td>
        </tr>
      </table>
    </form>
  </div>
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/html5shiv-printshiv.js"></script>
    <script src="js/unit.js"></script>
    <script src="js/validation.js" type="text/javascript"></script>
  </body>
  </html>




